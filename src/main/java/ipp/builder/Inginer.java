package ipp.builder;

public class Inginer {
    private PcBuilder pcBuilder;

    public void setPcBuilder(PcBuilder pcBuilder) {
        this.pcBuilder = pcBuilder;
    }

    public Pc getPc() {
        return pcBuilder.getPc();
    }

    public void constructPc() {
        pcBuilder.createNewPc();
        pcBuilder.buildName();
        pcBuilder.buildProcessor();
        pcBuilder.buildAmount();
        pcBuilder.buildGen();
        pcBuilder.buildVideoCard();
    }
}