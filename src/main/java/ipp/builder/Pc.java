package ipp.builder;

public class Pc {
    private String name;
    private String processor;
    private int amount;
    private String gen;
    private String videoCard;

    public void setName(String name) {
        this.name = name;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }

    public void setVideoCard(String videoCard) {
        this.videoCard = videoCard;
    }


    public String getProcessor() {
        return processor;
    }

    public int getAmount() {
        return amount;
    }

    public String getGen() {
        return gen;
    }

    public String getVideoCard() {
        return videoCard;
    }

    public String getName() {
        return name;
    }

    public void printPc() {
        System.out.println(" *** " + name + "***\n" +
                " Processor: " + processor + "\n" +
                " Amount: " + amount + "\n" +
                " Gen: " + gen + "\n" +
                " VideoCard: " + videoCard + "\n");
    }
}
