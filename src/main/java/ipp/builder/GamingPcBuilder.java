package ipp.builder;

public class GamingPcBuilder extends PcBuilder{

        @Override
        public void buildName() {
            pc.setName("Asus Rog");
        }

        @Override
        public void buildProcessor() {
            pc.setProcessor("Intel i7");
        }

        @Override
        public void buildAmount() {
            pc.setAmount(19);
        }

        @Override
        public void buildGen() {
            pc.setGen("7");
        }

        @Override
        public void buildVideoCard() {
            pc.setVideoCard("1050Ti");
        }

}
