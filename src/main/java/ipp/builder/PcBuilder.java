package ipp.builder;

public abstract class PcBuilder {

        protected Pc pc;

        public Pc getPc() {
            return pc;
        }

        public void createNewPc() {
            pc = new Pc();
        }

        public abstract void buildName();

        public abstract void buildProcessor();

        public abstract void buildAmount();

        public abstract void buildGen();

        public abstract void buildVideoCard();


}
