package ipp.builder;

public class WorkPcBuilder extends PcBuilder {

        @Override
        public void buildName() {
            pc.setName("Hp");
        }

        @Override
        public void buildProcessor() {
            pc.setProcessor("Intel i7");
        }

        @Override
        public void buildAmount() {
            pc.setAmount(19);
        }

        @Override
        public void buildGen() {
            pc.setGen("7");
        }

        @Override
        public void buildVideoCard() {
            pc.setVideoCard("Intel Graphics");
        }

}
