package ipp.prototype;

public class Apple extends Pc {

    public Apple()
    {
        this.pcType = "Apple";
    }

    @Override
    public void addPc() {
        System.out.println("Apple added to shop");
    }
}
