package ipp.prototype;

public class Asus extends Pc {

    public Asus()
    {
        this.pcType = "Asus";
    }

    @Override
    public void addPc() {
        System.out.println("Asus added to shop");
    }
}
