package ipp.prototype;

public class Hp extends Pc {

    public Hp()
    {
        this.pcType = "Hp";
    }

    @Override
    public void addPc() {
        System.out.println("Hp added to shop");
    }
}
