package ipp.prototype;

public abstract class Pc implements Cloneable {

    protected String pcType;

    public abstract void addPc();

    public Object clone() {
        Object clone = null;
        try {
            clone = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }
}
