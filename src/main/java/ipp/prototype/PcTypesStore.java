package ipp.prototype;

import java.util.HashMap;
import java.util.Map;

public class PcTypesStore {

    private static Map<String, Pc> pcMap = new HashMap<String, Pc>();

    static {
        pcMap.put("Asus", new Asus());
        pcMap.put("Apple", new Apple());
        pcMap.put("Hp", new Hp());
    }

    public static Pc getPc(String pcName) {
        return (Pc) pcMap.get(pcName).clone();
    }
}