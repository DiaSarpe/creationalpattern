package ipp;

import ipp.prototype.PcTypesStore;
import ipp.singleton.*;
import ipp.factory_method.*;
import ipp.abstract_factory.*;
import ipp.builder.*;

public class Client {
    public static void main(String[] args) {
        System.out.println("\n***** Welcome To 999 Shop *****\n");
        System.out.println("--- SINGLETON");
        ComputersShopWebsite website = ComputersShopWebsite.getInstance();
        website.showWebsite();

        System.out.println("\n--- FACTORY METHOD");
        Computers computers = ComputersParametersFactory.getParametersOfComputers("Asus");
        computers.parameter("2017", "i7");
        computers = ComputersParametersFactory.getParametersOfComputers("HP");
        computers.parameter("2016", "i5");
        computers = ComputersParametersFactory.getParametersOfComputers("DELL");
        computers.parameter("2014", "i3");

        System.out.println("\n--- ABSTRACT FACTORY");
        ComputersFactory factory1 = ComputerMarker.getFactory("PC");

        PersonalComputers pc1 = factory1.createPersonalComputers();//personal
        pc1.getPerson("Children");
        WorkstationComputers pc2 = factory1.createWorkstationComputers();
        pc2.getPerson("Parents");

        ComputersFactory factory2 = ComputerMarker.getFactory("Workstation");//office
        PersonalComputers pc3 = factory1.createPersonalComputers();//personal
        pc3.getPerson("Children");
        WorkstationComputers pc4 = factory1.createWorkstationComputers();
        pc4.getPerson("Parents");


        System.out.println("\n--- BUILDER");
        Inginer inginer= new Inginer();
        PcBuilder gammingPcBuilder = new GamingPcBuilder();
        PcBuilder workPcBuilder = new WorkPcBuilder();

        inginer.setPcBuilder(gammingPcBuilder);
        inginer.constructPc();
        Pc pc5 = inginer.getPc();
        pc5.printPc();

        inginer.setPcBuilder(workPcBuilder);
        inginer.constructPc();
        Pc pc6 = inginer.getPc();
        pc6.printPc();

        System.out.println("--- PROTOTYPE");
        PcTypesStore.getPc("Apple").addPc();
        PcTypesStore.getPc("Hp").addPc();
        PcTypesStore.getPc("Asus").addPc();
        PcTypesStore.getPc("Hp").addPc();
        PcTypesStore.getPc("Apple").addPc();
    }
}