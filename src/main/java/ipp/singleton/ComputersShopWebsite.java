package ipp.singleton;

public class ComputersShopWebsite {

    private static ComputersShopWebsite website = new ComputersShopWebsite();

    // A private Constructor prevents any other class from instantiating.
    private ComputersShopWebsite() {
    }

    // Static 'instance' method
    public static ComputersShopWebsite getInstance() {
        return website;
    }

    // Other methods protected by singleton-ness
    public static void showWebsite() {
        System.out.println(" Computers for Everybody => www.999.com ");
    }
}