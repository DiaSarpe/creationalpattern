package ipp.factory_method;

public class Dell implements Computers{
    @Override
    public void parameter(String year, String processor) {
        System.out.println("Year:  " + year + "Processor:  " + processor);
    }
}
