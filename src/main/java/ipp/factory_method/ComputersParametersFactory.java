package ipp.factory_method;

public class ComputersParametersFactory {
    public static Computers getParametersOfComputers(String criteria) {
        if (criteria.equals("Asus"))
            return new Asus();
        else if (criteria.equals("HP"))
            return new Hp();
        else if (criteria.equals("DELL"))
            return new Dell();
        return null;
    }
}
