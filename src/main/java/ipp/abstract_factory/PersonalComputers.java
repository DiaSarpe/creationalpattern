package ipp.abstract_factory;

public interface PersonalComputers {
    void getPerson(String person);
}
