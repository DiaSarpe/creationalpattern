package ipp.abstract_factory;

public class Imac implements WorkstationComputers {

    Imac() {
        System.out.println(" Workstation Computers");
    }

    @Override
    public void getPerson(String person) {
        System.out.println(" used by " + person );
    }
}
