package ipp.abstract_factory;

class WindowsComputerFactory extends ComputersFactory{
    @Override
    public WorkstationComputers createWorkstationComputers() {
        return new Asus();
    }

    @Override
    public PersonalComputers createPersonalComputers() {
        return new Hp();
    }
}
