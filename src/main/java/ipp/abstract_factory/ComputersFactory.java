package ipp.abstract_factory;

public abstract class ComputersFactory {
    public abstract WorkstationComputers createWorkstationComputers();
    public abstract PersonalComputers createPersonalComputers();
}
