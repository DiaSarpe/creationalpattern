package ipp.abstract_factory;

public class ComputerMarker {
    private static ComputersFactory factory = null;

    public static ComputersFactory getFactory(String choice) {
        if (choice.equals("PC")) {
            factory = new MacComputerFactory();
        } else if (choice.equals("Workstation")) {
            factory = new WindowsComputerFactory();
        }
        return factory;
    }
}
