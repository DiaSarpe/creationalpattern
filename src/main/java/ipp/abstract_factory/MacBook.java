package ipp.abstract_factory;

public class MacBook implements PersonalComputers {

    MacBook() {
        System.out.println(" Personal Computer MacBook");
    }

    @Override
    public void getPerson(String person) {
        System.out.println(" used by " + person );
    }
}
