package ipp.abstract_factory;

class MacComputerFactory extends ComputersFactory{
    @Override
    public WorkstationComputers createWorkstationComputers() {
        return new Imac();
    }

    @Override
    public PersonalComputers createPersonalComputers() {
        return new MacBook();
    }
}
