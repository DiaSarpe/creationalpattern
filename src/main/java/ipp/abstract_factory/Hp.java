package ipp.abstract_factory;

public class Hp implements PersonalComputers {

    Hp() {
        System.out.println(" Personal Computers");
    }

    @Override
    public void getPerson(String person) {
        System.out.println(" used by " + person );
    }
}
