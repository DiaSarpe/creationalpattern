package ipp.abstract_factory;

public class Asus implements WorkstationComputers {

    Asus() {
        System.out.println(" Workstation Computers");
    }

    @Override
    public void getPerson(String person) {
        System.out.println(" used by " + person );
    }
}
